 

// ************************* PINS *************************
#define tel_pin A2

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(tel_pin, INPUT);
}

void loop() {
  int val = analogRead(tel_pin);
  Serial.println(val);
  delay(500);

}
