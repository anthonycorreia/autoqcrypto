#include <Arduino.h>
#include "A4988.h"
// ************************* PINS *************************

// Moteur
#define dir_pin 2
#define step_pin 3

#define dir_pin2 4
#define step_pin2 5

// Pin à mettre à 0 si le moteur est inactif
// pour le mettre en mode 'sleep'
#define active_motor_pin 6

// ******************* Global variables *******************
int steps_per_revolution = 200 ; // nombre de steps / révolution
int stepCount = 0;
int rpm = 10;

A4988 stepper(steps_per_revolution, dir_pin, step_pin);
A4988 stepper2(steps_per_revolution, dir_pin2, step_pin2);

void setup() {
  Serial.begin(9600);
  
  stepper.begin(rpm);
  stepper2.begin(rpm);

}

int i = 7;
void loop() {
  if (Serial.available()) {i = Serial.read() - 48; Serial.read();}
  Serial.println(i);
  
  if (i < 7){
    stepper.rotate(-175); // 170
  }
  i+=1;

  delay(1000);
  


}
