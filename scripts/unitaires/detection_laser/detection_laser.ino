 

// ************************* PINS *************************
#define det_pin0 A0
#define det_pin1 A1

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(det_pin0, INPUT);
  pinMode(det_pin1, INPUT);  
}

void loop() {
  int val0 = analogRead(det_pin0);
  int val1 = analogRead(det_pin1);
  Serial.println(val0);
  Serial.println(val1);
  // if (val > 200) {Serial.println("Laser détecté");}
  Serial.println("----------------");
  delay(500);

}
