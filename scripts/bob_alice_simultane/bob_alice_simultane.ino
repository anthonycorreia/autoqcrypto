/* CODE ARDUINO DE ALICE ET BOB - PROJET QUANTIQUE
Date: 28 Jan. 2022
Auteur: Anthony Correia

Les bases de mesure sont notées:
- `0` ou `false` pour la base +
- `1` ou `true` pour la base x

Arduino communique via la voie Serial, à une vitesse de 9600 bauds.

Instructions à envoyer à l'Arduino via le Serial
------------------------------------------------

* `MA {base}{bit}` avec `base` et `bit` vallant 0 ou 1 
    pour le contrôle du moteur d'Alice.
* `MB {base}` avec `base` vallant 0 ou 1 
    pour le contrôle du moteur de Bob.
* `DB` pour demander de mesurer le bit mesuré par les détecteurs
    de Bob. Le résultat est renvoyé via le Serial.
* `LA {mode}` avec `mode` à 0 ou 1 selon que le laser soit éteint
    ou allumé. On aurait pu considéré `LA` comme un switch, mais
    je pense que cette implémentation est plus robuste.

Instructions envoyées par l'Arduino
-----------------------------------

* Si une mesure lui est demandée via la commande `DB`, l'Arduino envoie 
    le résultat de la mesure sous la forme `DB {D0}{D1}` 
    avec D0 et D1 vallant 0 ou 1, selon que le détecteur 0 et 1 de Bob
    détecte ou non le laser.
* Après avoir réalisée une instruction, l'Arduino envoie `ST` (stop) pour
    indiquer que l'action a abouti.
*/

// *****************************************************************
// *********************** INITIALISATION **************************
// *****************************************************************

#include <Arduino.h>

//  **************************** PINS ****************************
// Moteur ------------------------------------------------
// Alice
#define dir_pin_A 4
#define step_pin_A 5

// Bob
#define dir_pin_B 2
#define step_pin_B 3

// Detecteurs --------------------------------------------
// Bob
#define det_pin_B0 A0 // Détecteur 0 de Bob
#define det_pin_B1 A1 // Détecteur 1 de Bob

// Laser ------------------------------------------------
// Alice
#define laser_pin_A 6

// ******************** Variables globales **********************
// moteur -----------------------------------------------
// int pas_par_revolution = 200 ; // nombre de pas / révolution (360°)
// int rpm = 10; // nombre de tour / minute
// nombre de pas / min : pas_par_revolution * rpm
// min / pas = 1 / (200 * 10)
// s / pas = 60 / (200 * 10) = 0.03 s = 30 ms

//double time_per_pas_ms = 30.;// temps entre chaque pas en ms
double time_per_pas_ms = 10.;// temps entre chaque pas en ms

int ZM = 35 ; // nombre de dents du pignon moteur
int ZL = 270 ; // nombre de dents du pignon côté lame lambda/2

// Calculs ...
// double angleM_par_45deg =  185; // angle côté moteur à effectuer 
int pasM_par_45deg = 185 ; // pas côté moteur à effectuer(défaut : 175) 185 trop faible, 210 trop grand
// 175° côté moteur
// =>  pasM_par_45deg = 200 * 175 / 360 = 97 pas

// pour que la plaque lambda/2 tourne de 45°

// Bases de mesure --------------------------------------
// Alice
bool base_A = false;
bool bit_A = false;
// Bob
bool base_B = false;

// Pour instructions (mis à jour par une instruction)
// Alice
bool base_A_new = false;
bool bit_A_new = false;
// Bob
bool base_B_new = false;

// Laser -----------------------------------------------
bool laser_A = false;

// Mesure ----------------------------------------------
int seuil_detection = 210;

// Etat
char etat[] = "ST";

// *****************************************************************
// ************************* FONCTIONS *****************************
// *****************************************************************

// ************** (base, bin) -> Degrés *******************

int n45deg_lame_into_pas_mot(int n45deg_lame) {
  /* Convertit un angle de rotation côté lame en un angle
  de rotation côté moteur.

  Paramètres
  ----------
  n45deg_lame: angle de rotation de lame à retard,
    en 45°. L'angle de rotation est donc `n45deg * 45°`.
  
  Return
  ------
  angle_mot (double): angle de moteur correspondant à
    une rotation de `n45deg * 45` degré côté lame à retard. 
  */
  return n45deg_lame * pasM_par_45deg ;
}

int base_bit_into_n45deg(bool base_lame, bool bit_lame) {
  /* Associe au couple `(base, bit)` l'angle
  de rotation de lame à retard.
  
  Paramètres
  ----------
  base_lame, bit_lame (bool): base et bit qu'encode actuellement
    la plaque lambda/2
  
  Return
  ------
  n45deg_lame (int): angle de rotation de lame à retard,
    en 45°. L'angle de rotation est donc `n45deg * 45°`.
  */

  if (!base_lame and !bit_lame) { return 0;} // (+, 0)
  else if (!base_lame and bit_lame) { return 2;} // (+, 1)
  else if (base_lame and !bit_lame) { return -1;} // (X, 0)
  else if (base_lame and bit_lame) { return 1;} // (X, 1)
}

int get_pas_mot_A(
  bool base_in, bool bit_in, 
  bool base_out, bool bit_out) {
  /* Obtenir l'angle de rotation du moteur à effectuer pour que 
  Alice passe du couple (base_in, bin_in) au couple
  (base_out, bit_out).

  Paramètres
  ----------
  base_in, bit_in (bool): base et bit qu'encode actuellement
    la plaque lambda/2
  base_out, bit_out (bool): base et bit à encoder

  Return
  ------
  angle_mot (double): angle de rotation à effectuer par le moteur
  */

  // angle in et out de la lame
  int angle_lame_in = base_bit_into_n45deg(base_in, bit_in);
  int angle_lame_out = base_bit_into_n45deg(base_out, bit_out);

  // Rotation à effectuer  
  return n45deg_lame_into_pas_mot(angle_lame_out - angle_lame_in);
}

int get_pas_mot_B(
  bool base_in, bool base_out) {
  /* Obtenir l'angle de rotation du moteur à effectuer pour que 
  Bob passe de la base `base_in` à la base `base_out`

  Paramètres
  ----------
  base_in, base_out (bool): base initiale et finale

  Return
  ------
  angle_mot (double): angle de rotation à effectuer par le moteur
  */

  return - get_pas_mot_A(
    false, base_in, 
    false, base_out
  ) / 2;
}

// ************************ Mesure *****************************

const char *mesure_bob() {
  /* Retourne le résultat de la mesure de Bob. Le seuil
  de détection est donné par la variable globale 
  `seul_detection`.

  Return
  ------
  mes_B (String): vaut `"{i1}{i2}"` avec `i1` et `i2`
    valant 0 ou 1 selon que le détecteur 1 et 2
    détecte (1) ou non (0) un singal
  */

  // Détection par les 2 détecteurs
  bool det0 = analogRead(det_pin_B0) > seuil_detection;
  bool det1 = analogRead(det_pin_B1) > seuil_detection;

  if (!det0 and !det1) { return "00";}
  else if (!det0 and det1) { return "01";}
  else if (det0 and !det1) { return "10";}
  else if (det0 and det1) { return "11";}
}


// ********************** Instructions **************************

void dump_serial() {
  /* Ignore ce qui a été écrit dans le Serial. */
  while (Serial.available() > 0) {
    Serial.read();
  }
}

bool char_est_zero_ou_un(char caractere) {
  /* Vérifie qu'un char soit bien 0 ou 1.

  Paramètres
  ----------
  caractere (char): un caractère

  Return
  ------
  est_zero_ou_un (bool): est-ce que `caractere`
    vaut `'0'` ou `'1'` ? 
  */
  return (caractere=='0') or (caractere=='1');
}

bool char_into_bool(char zero_ou_un) {
  /* Convertit un caractère vallant 0 ou 1,
  en un booléan valant `false` ou `true`.

  Paramètres
  ----------
  zero_ou_un (char): vaut `'0'` ou `'1'`

  Return
  ------
  zero_ou_un_bool (bool): `false` ou `true` 
  */
  if (zero_ou_un=='0') {
    return false;
  } else if (zero_ou_un=='1') {
    return true;
  }
}

bool lire_char_zero_ou_one(bool *var) {
  /* Lit un caractère du Serial. Si ce caractère
   est `'0'` ou `'1', met à jour la variable `var`.

   Paramètres
   ----------
   var (bool): variable qui sera mis à jour par
    la fonction.

  Return
  ------
  valid: le caractère lu était bien un 0 ou un 1
  */
  char car = Serial.read();
  bool valide = char_est_zero_ou_un(car);
  if (valide) {
    *var = char_into_bool(car);
  }

  return valide;
}

bool check_chars_egal(char *chars1, char *chars2) {
  /* Verifie que 2 chaînes de caractères de 2 lettres
  soient égales.

  Paramètres
  ----------
  chars1, chars2: 2 chaînes de caractère

  Return
  ------
  (bool) si les 2 chaînes de caractère sont les mêmes.
  */
  return (chars1[0]==chars2[0] and chars1[1]==chars2[1] and chars1[2]==chars2[2]);
}

void lire_instruction(
  char *etat,
  bool *base_A_new, bool *bit_A_new, bool*base_B_new,
  bool *laser_A) {
  /* Lit une commande via le Serial et met à jour les pointers
   mis en paramètre.

  Paramètres
  ----------
  etat (str): état de la machine d'état
  base_A_new, bit_A_new (bool): base et bit de la lame d'Alice
  base_B_new (bool): base de la lame de Bob
  laser_A (bool): Etat du laser d'Alice (allumé [1] ou éteint [0])

  Return
  ------
  commande (String): commande envoyée via le Serial.
  */
  
  // delay pour que la commande complète est le temps d'arriver
  delay(100); 
  char instruction[3];
  bool valid = false; // is the instruction valid?
  
  if (Serial.available() >= 2) {
    // Lecture de la commande
    instruction[0] = Serial.read(); 
    instruction[1] = Serial.read();
    instruction[2] = '\0';
    
    if (check_chars_egal(instruction, (char *) "MA")) {
      if (Serial.available() >= 3) {
        Serial.read(); // Sauter l'espace
        valid = (lire_char_zero_ou_one(base_A_new) and lire_char_zero_ou_one(bit_A_new));
        
      }
    } else if (check_chars_egal(instruction, (char *) "MB")) {
      if (Serial.available() >= 2) {
        Serial.read(); // Sauter l'espace
        valid = lire_char_zero_ou_one(base_B_new);
      }
    } else if (check_chars_egal(instruction, (char *) "MC")) {
      if (Serial.available() >= 2) {
        Serial.read(); // Sauter l'espace
        valid = (lire_char_zero_ou_one(base_A_new) and lire_char_zero_ou_one(bit_A_new));
        Serial.read();
        valid &= lire_char_zero_ou_one(base_B_new);
      }
    } else if (check_chars_egal(instruction, (char *) "LA")) {
      if (Serial.available() >= 2) {
        Serial.read(); // Sauter l'espace
        valid = lire_char_zero_ou_one(laser_A);
      }
    } else if (check_chars_egal(instruction, (char *) "DB")) {
      dump_serial();
      valid = true;
    }
  }
  dump_serial();

  if (valid) {
    etat[0] = instruction[0];
    etat[1] = instruction[1];
  }
}

// **************************** MAIN ****************************

void setup() {
  // Serial -----------------------
  Serial.begin(9600);

  // pins ------------------------
  // Detecteurs
  pinMode(det_pin_B0, INPUT);
  pinMode(det_pin_B1, INPUT);
  // Laser
  pinMode(laser_pin_A, OUTPUT);
  digitalWrite(laser_pin_A, LOW);
  // Moteurs
  pinMode(dir_pin_A, OUTPUT);
  pinMode(step_pin_A, OUTPUT);
  pinMode(dir_pin_B, OUTPUT);
  pinMode(step_pin_B, OUTPUT);
  digitalWrite(step_pin_A, LOW);
  digitalWrite(step_pin_B, LOW);
  digitalWrite(dir_pin_A, LOW);
  digitalWrite(dir_pin_B, LOW);
}


// VARIABLES MOTEURS =======================================
// Etat de dir_pin_A et dir_pin_B
bool state_step_pin_A = LOW;
bool state_step_pin_B = LOW;
// Temps pour lequel l'état d'une des pin de step
// a été intervertit
unsigned long time_step_pin = millis();
// Nombre de pas restant à effectuer
int pas_A = 0;
int pas_B = 0;
// ========================================================

void loop() {
  // Serial.print("etat: ");
  // Serial.println(etat);

  if (check_chars_egal(etat, (char *) "MA") | check_chars_egal(etat, (char *) "MB") | check_chars_egal(etat, (char *) "MC")) {
    // Serial.println(etat);
    // Calcul nombre de pas à effectuer
    // Serial.println(base_A);
    // Serial.println(bit_A);
    // Serial.println(base_A_new);
    // Serial.println(bit_A_new);
    // Serial.println(pas_A);
    // Serial.println(pas_B);
    // Serial.println("------");
    if ((base_A!=base_A_new) or (bit_A!=bit_A_new)) { // moteur Alice
      pas_A = get_pas_mot_A(
        base_A, bit_A, base_A_new, bit_A_new
      );
      Serial.print("pas ");
      Serial.println(pas_A);
      base_A = base_A_new;
      bit_A  = bit_A_new;

      // Direction
      if (pas_A < 0) {
        digitalWrite(dir_pin_A, LOW);
        pas_A = - pas_A;
      } else {
        digitalWrite(dir_pin_A, HIGH);
      }
    }
    
    if (base_B!=base_B_new){ // moteur Bob
      pas_B = get_pas_mot_B(
        base_B, base_B_new
      );
      base_B = base_B_new;

      // Direction
      if (pas_B < 0) {
        digitalWrite(dir_pin_B, LOW);
        pas_B = - pas_B;
      } else {
        digitalWrite(dir_pin_B, HIGH);
      }
    }
    
    if ((millis() - time_step_pin) > time_per_pas_ms / 2) {
      if (pas_A!=0){
        state_step_pin_A = !state_step_pin_A;
        digitalWrite(step_pin_A, state_step_pin_A);
        pas_A--;
      }
   
      if (pas_B!=0){ 
        state_step_pin_B = !state_step_pin_B;
        digitalWrite(step_pin_B, state_step_pin_B);

        pas_B--;
      }
      time_step_pin = millis();
    }

    if ((pas_A==0) and (pas_B==0)) {
      
      etat[0] = 'S';
      etat[1] = 'T';
      Serial.println("ST");
    }
    
  } else {
    lire_instruction(
      etat,
      &base_A_new, &bit_A_new, // Lame d'Alice
      &base_B_new, // Lame de Bob
      &laser_A
    );
  
    if (check_chars_egal(etat, (char *) "DB")) { // Detection Bob
    Serial.println(mesure_bob());
    
    etat[0] = 'S';
    etat[1] = 'T';
    
  } else if (check_chars_egal(etat, (char *) "LA")) { // Laser Alice
      digitalWrite(laser_pin_A, laser_A);
      // Serial.print("Laser ");
      // Serial.println(laser_A);
      
      etat[0] = 'S';
      etat[1] = 'T';
    }
  }
  
}
