import numpy as np
import sys
import serial
import time
ser = serial.Serial(baudrate=9600)

# # Connexion à l'arduino
port = "COM5" # ou COM6, ou COM7, ...
ser.port = port
ser.open()
time.sleep(2)

# # Envoie de messages à l'Arduino
# message = "BA 01"
# ser.write(bytes(message, 'UTF-8'))

# # Lecture de messages envoyés par l'Arduino
# while ser.in_waiting > 0: # tant que des messages sont reçus, peut-être qu'un if est plus adapté ici
#     message = ser.readline()[:-2].decode() # format str
#     # Je ne sais plus pourquoi j'ai mis -2
#     # peut-être que le dernier bit est inutile

def n2s(n,l=2):
    s = str(n)
    s = " "*(l-len(s)) + s
    return s

n= int(sys.argv[1]) if len(sys.argv)>1 else 50
gen = np.random.default_rng()
alicebasis = gen.integers(2,size=n)
bits = gen.integers(2,size=n)
print("Alice")
print(*map(lambda x:n2s(x,3),range(n)),sep="|")
print(*[" "*2+"+x"[b] for b in alicebasis],sep="|")
print(*map(lambda x:n2s(x,3),bits),sep="|")
print(*[f"{[0,90,-45,45][alicebasis[i]*2+bits[i]]: <3}" for i in range(len(alicebasis))],sep="|")
# bbasis = input("Bob's basis ?")
# print(list(bbasis))
# print(["+x"[b] for b in basis])
# indexes = np.where(np.array(list(bbasis))==np.array(["+x"[b] for b in basis]))
# print(indexes)
# print("Key : ",*bits[indexes],sep="")

def n2s(n,l=2):
	s = str(n)
	s = " "*(l-len(s)) + s
	return s

n= int(sys.argv[1]) if len(sys.argv)>1 else 50
gen = np.random.default_rng()
bobbasis = gen.integers(2,size=n)
print("Bob")
print(*map(lambda x:n2s(x,2),range(n)),sep="|")
print(*[" "+"+x"[b] for b in bobbasis],sep="|")
print(*[[" 0","45"][b] for b in bobbasis],sep="|")

# bits = list(map(int,input("Bits received ? ")))
# abasis = input("Alice's basis ? ")
# indexes = np.where(np.array(list(abasis))==np.array(["+x"[b] for b in basis]))
# print("Key : ",*np.array(bits)[indexes],sep="")

def wait_ST():
    ST_received = False
    while not ST_received:
        if ser.in_waiting > 0:
            receivedmessage = ser.readline()[:-2].decode()
            ST_received = (receivedmessage=="ST")

receivedBits = []
for i in range(n):
    print("Alice :", [0,90,-45,45][alicebasis[i]*2+bits[i]])
    # ser.write(bytes(f"MA {alicebasis[i]}{bits[i]}", 'UTF-8')) 
    # wait_ST()

    print("Bob :",[0,45][bobbasis[i]])
    # ser.write(bytes(f"MB {bobbasis[i]}", 'UTF-8'))
    ser.write(bytes(f"MC {alicebasis[i]}{bits[i]} {bobbasis[i]}", 'UTF-8'))
    wait_ST()

    input("Press enter to fire laser...")
    ser.write(bytes("LA 1", 'UTF-8'))
    time.sleep(0.5)
    ser.write(bytes(f"DB", 'UTF-8'))
    time.sleep(1)
    while ser.in_waiting > 0:
        receivedmessage = ser.readline()[:-2].decode()
    
    print(receivedmessage)
    if receivedmessage == "01":
        receivedBit = 1
    elif receivedmessage == "10":
        receivedBit = 0
    elif receivedmessage == "11":
        receivedBit = gen.integers(2)
    else:
        print(f"Error on bit {i}")
        
    receivedBits.append(receivedBit)
    ser.write(bytes("LA 0", 'UTF-8'))
    time.sleep(0.5)

print(receivedBits)

receivedBits = np.array(receivedBits)
print("Identical bits :",*np.where(alicebasis==bobbasis)[0])
print("Key Alice :",bits[np.where(alicebasis==bobbasis)])
print("Key Bob :  ",receivedBits[np.where(alicebasis==bobbasis)])
different = np.sum(np.abs(receivedBits[np.where(alicebasis==bobbasis)]-bits[np.where(alicebasis==bobbasis)]))
print(f"Number of non-matching bits : {different} over {len(np.where(alicebasis==bobbasis)[0])} ({different/len(np.where(alicebasis==bobbasis)[0]) * 100:.1f} %)")
